package com.github.abbalcerek.bookcatalog.controller;

import com.github.abbalcerek.bookcatalog.model.Book;
import com.github.abbalcerek.bookcatalog.service.BookService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;


@RunWith(SpringRunner.class)
//whole context
@SpringBootTest
@AutoConfigureMockMvc
//only single controller
//@WebMvcTest(BookController.class)
public class BookControllerTest {

    @MockBean
    private BookService bookService;

    @Autowired
    private MockMvc mockMvc;


    @Test
    public void indexTest() throws Exception {

        mockMvc.perform(get("/"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:book-list"))
                .andExpect(redirectedUrl("book-list"));
    }

    @Test
    public void bookDetailTest() throws Exception {

        Long id = 99L;
        String title = "title1";
        String isbn = "isbn1";
        String author = "author1";
        String description = "description1";
        Integer year = 9999;
        Book book = new Book(id, title, isbn, author, description, year);

        when(bookService.book(id)).thenReturn(book);

        mockMvc.perform(get("/book-detail/" + id))
                .andExpect(status().isOk())
                .andExpect(view().name("book-detail"))
                .andExpect(model().attributeExists("book"))
                .andExpect(model().attribute("book", hasProperty("id", is(id))))
                .andExpect(model().attribute("book", hasProperty("title", is(title))))
                .andExpect(model().attribute("book", hasProperty("isbn", is(isbn))))
                .andExpect(model().attribute("book", hasProperty("author", is(author))))
                .andExpect(model().attribute("book", hasProperty("description", is(description))))
                .andExpect(model().attribute("book", hasProperty("year", is(year))));

        verify(bookService, times(1)).book(id);
    }

    @Test
    public void saveBookTest() throws Exception {

        Long id = 99L;
        String title = "title1";
        String isbn = "isbn1";
        String author = "author1";
        String description = "description1";
        Integer year = 9999;
        Book book = new Book(id, title, isbn, author, description, year);

        when(bookService.book(id)).thenReturn(book);

        mockMvc.perform(
                post("/save-book/")
                        .param("id", Long.toString(id))
                        .param("title", title)
                        .param("isbn", isbn)
                        .param("author", author)
                        .param("description", description)
                        .param("year", Integer.toString(year))
        )
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/book-list"))
                .andExpect(redirectedUrl("/book-list"));

        // this requires correct equal and hashcode on the dto object
        verify(bookService, times(1)).addBook(book);
    }

    @Test
    public void bookListTest() throws Exception {

        mockMvc.perform(get("/book-list"))
                .andExpect(status().isOk())
                .andExpect(view().name("book-list"));

        verify(bookService, times(1)).books();
    }

}
