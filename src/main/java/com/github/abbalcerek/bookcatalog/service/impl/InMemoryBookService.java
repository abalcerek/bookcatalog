package com.github.abbalcerek.bookcatalog.service.impl;

import com.github.abbalcerek.bookcatalog.model.Book;
import com.github.abbalcerek.bookcatalog.service.BookService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
class InMemoryBookService implements BookService {

    private Map<Long, Book> books;

    {
        books = new HashMap<>();
        books.put(1L, new Book(1L,
                "Python dla każdego. Podstawy programowania",
                "45234523",
                "Dawson Michael",
                "Chcesz się nauczyć programować? Świetna decyzja! Wybierz język obiektowy, łatwy w użyciu, " +
                        "z przejrzystą składnią. Python będzie wprost doskonały! Rozwijany od ponad 20 lat, " +
                        "jest dojrzałym językiem, pozwalającym tworzyć zaawansowane aplikacje dla różnych systemów " +
                        "operacyjnych. Ponadto posiada system automatycznego zarządzania pamięcią, który zdejmuje " +
                        "z programisty obowiązek panowania nad tym skomplikowanym obszarem.",
                2014));

        books.put(2L, new Book(2L,
                "Czysty kod. Podręcznik dobrego programisty",
                "54325342",
                "Robert C. Martin",
                "Poznaj najlepsze metody tworzenia doskonałego kodu",
                2014));
        books.put(3L, new Book(3L,
                "Programista samouk. Profesjonalny przewodnik do samodzielnej nauki kodowania",
                "634565",
                "Althoff Cory",
                "Nie wystarczy znajomość jednego języka programowania, aby zostać programistą. " +
                        "W rzeczywistości trzeba opanować dość szeroki zakres pojęć i paradygmatów, a także zrozumieć " +
                        "zagadnienia związane z algorytmami. Trzeba być na bieżąco z nowymi technologiami i narzędziami. " +
                        "Należy również poznać i zacząć stosować dobre praktyki programistyczne i przyswoić sobie zasady " +
                        "pracy w zespole. Przede wszystkim jednak priorytetem jest sama praktyka, ponieważ wielu programistów " +
                        "wciąż ma problem z pisaniem poprawnego kodu.",
                2017));
    }

    @Override
    public List<Book> books() {
        return new ArrayList<>(books.values());
    }

    @Override
    public Book book(Long id) {
        return books.get(id);
    }

    @Override
    public void removeBook(Long id) {
        books.remove(id);
    }

    @Override
    public Book addBook(Book book) {
        Long id = book.getId();
        if (book.getId() == null) {
            id = generateId();
        }
        book.setId(id);
        return books.put(id, book);
    }

    private Long generateId() {
        long max = 0;
        for (long key: books.keySet()) {
            max = Math.max(max, key);
        }
        return max + 1;
    }

}
