package com.github.abbalcerek.bookcatalog.service;

import com.github.abbalcerek.bookcatalog.model.Book;

import java.util.List;

public interface BookService {

    List<Book> books();

    Book book(Long id);

    void removeBook(Long id);

    Book addBook(Book book);

}
