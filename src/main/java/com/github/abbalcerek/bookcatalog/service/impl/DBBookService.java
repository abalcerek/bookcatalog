package com.github.abbalcerek.bookcatalog.service.impl;


import com.github.abbalcerek.bookcatalog.model.Book;
import com.github.abbalcerek.bookcatalog.repository.BookRepository;
import com.github.abbalcerek.bookcatalog.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Primary
@Transactional
@Service
public class DBBookService implements BookService {

    private final BookRepository bookRepository;

    @Autowired
    public DBBookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Book> books() {
        return bookRepository.findAll();
    }

    @Override
    public Book book(Long id) {
        return bookRepository.findById(id).orElse(null);
    }


    @Override
    public void removeBook(Long id) {
        bookRepository.deleteById(id);
    }

    @Override
    public Book addBook(Book book) {
        return bookRepository.save(book);
    }
}
