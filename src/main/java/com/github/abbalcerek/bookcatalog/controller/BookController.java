package com.github.abbalcerek.bookcatalog.controller;

import com.github.abbalcerek.bookcatalog.model.Book;
import com.github.abbalcerek.bookcatalog.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class BookController {

    private BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @PostMapping("save-book")
    public ModelAndView saveBook(@ModelAttribute Book book) {
        bookService.addBook(book);
        return new ModelAndView("redirect:/book-list");
    }

    @GetMapping("add-book")
    public ModelAndView updateBook() {
        ModelAndView modelAndView = new ModelAndView("book-form");
        modelAndView.addObject("book", new Book());
        return modelAndView;
    }

    @GetMapping("book-list")
    public ModelAndView bookList() {
        ModelAndView modelAndView = new ModelAndView("book-list");
        modelAndView.addObject("books", bookService.books());
        return modelAndView;
    }

    @GetMapping("book-detail/{id}")
    public ModelAndView bookDetail(@PathVariable Long id) {
        ModelAndView modelAndView = new ModelAndView("book-detail");
        modelAndView.addObject("book", bookService.book(id));
        return modelAndView;
    }

    @GetMapping("/")
    public ModelAndView index() {
        return new ModelAndView("redirect:book-list");
//        return new ModelAndView("forward:book-list");  //never add commented code :)
    }

    @PostMapping("delete-book")
    public ModelAndView deleteBook(@ModelAttribute Book book) {
        bookService.removeBook(book.getId());
        return new ModelAndView("redirect:/book-list");
    }

}
